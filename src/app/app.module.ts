import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, Tabs } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FavoritePage } from '../pages/favorite/favorite';
import { TabsPage } from '../pages/tabs/tabs';
import { LibraryPage } from '../pages/library/library';
import { QuotesPage } from '../pages/quotes/quotes';
import { SettingsPage} from '../pages/settings/settings';
import { QuotePage } from '../pages/quote/quote';
import { AddQuotePage } from '../pages/add-quote/add-quote';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FavoritePage,
    TabsPage,
    LibraryPage,
    QuotePage,
    QuotesPage,
    SettingsPage,
    AddQuotePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FavoritePage,
    TabsPage,
    LibraryPage,
    QuotePage,
    QuotesPage,
    SettingsPage,
    AddQuotePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
